//
//  AppDelegate.h
//  nutipodtest
//
//  Created by Jaak Laineste on 21/08/15.
//  Copyright (c) 2015 Jaak Laineste. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

