//
//  main.m
//  nutipodtest
//
//  Created by Jaak Laineste on 21/08/15.
//  Copyright (c) 2015 Jaak Laineste. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
